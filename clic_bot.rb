# frozen_string_literal: true

require "telegram/bot"

token = ENV["BOT_TOKEN"]

Telegram::Bot::Client.run(token) do |bot|
  bot.listen do |message|
    if message.new_chat_members != []
      bot.api.send_message(
        chat_id: message.chat.id,
        text:    "Bienvenide, #{message.from.first_name} o/. \
                 Te invitamos a leer el codigo de convivencia \
                 https://colectivo-de-livecoders.gitlab.io/#coc"
      )
    end

    case message.text
    when /open ?source/i
      bot.api.send_message(
        chat_id: message.chat.id,
        reply_to_message_id: message.message_id,
        text:    "no querrás decir software libre?"
      )
    when /ha(y|bria|bría) que/
      bot.api.send_message(
        chat_id: message.chat.id,
        reply_to_message_id: message.message_id,
        text:    "che que buena idea, por que no la haces?"
      )
    end
  end
end
